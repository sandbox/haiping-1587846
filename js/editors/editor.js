$(function(){
  $.widget("xtraform.editor", {
    options: {
      $container: null,
      $editor: null,
      fieldTemplate: null,
      editorStatus: "new",
      delayPost: false,
      // clone of all source field data
      fieldDatas: null,
      // store all column field data
      fieldDatasTemp: null
    },

    _create: function() {
      var editor = this;
      if(this.options.fieldTemplate.multiple == 1) {
        this.options.$editor = $('<div data-role="collapsible" data-collapsed="false" data-content-theme="c">' + '<h3>' + this.options.fieldTemplate.name + '</h3><ul data-role="listview" data-inset="true"></ul></div>');
        this.options.$editor.append(this._drawEditor());
      } else {
        this.options.$editor = this._drawEditor();
      }

      if($.isFunction(this._afterDrawEditor)) {
        this._afterDrawEditor();
      }

      if(this.options.fieldTemplate.multiple == 1) {
        this.options.$editor.append('<div data-role="controlgroup" data-type="horizontal">\
<button id="remove" data-icon="delete" data-mini="true">Remove</button>\
<button id="add" data-icon="plus" data-mini="true">Add</button>\
<button id="save" data-icon="check" data-mini="true">OK</button>\
</div>');
      }

      this.options.$editor.appendTo(this.options.$container);
    },

    _init: function(){
      var editor = this;

      this.options.fieldDatasTemp = this._initFieldDatasTemp();

      if(this.options.fieldTemplate.multiple == 1){
        this.options.$editor
        .find("#save").click(function(){ editor._onSaveButtonClick(); }).end()
          .find("#add").click(function(){ editor._onAddButtonClick(); }).end()
          .find("#remove").click(function(){ editor._onRemoveButtonClick(); });
      }

      if($.isFunction(this._registerEvents)) {
        this._registerEvents();
      }
      if(this.options.fieldDatas != null &&
        this.options.fieldDatas.length > 0) {
        if(this.options.fieldTemplate.multiple == 1) {
          var rowIds = new Array();
          $.each(this.options.fieldDatas, function(i) {
            rowIds.push(this.row);
          });
          rowIds = rowIds.unique();
          $.each(rowIds, function(i) {
            var fieldColumnDatas = findDataByRow(editor.options.fieldDatas, editor.options.fieldTemplate.id, rowIds[i]);
            editor._appendRow(fieldColumnDatas, rowIds[i]);
          });
        } else {
          $.extend(this.options.fieldDatasTemp, this.options.fieldDatas);
            editor.setEditorValue(this.options.fieldDatasTemp);
        }
      }

      if($.isFunction(this._renderEditorComplete)) {
        this._renderEditorComplete();
      }
    },

    _onValueChanged: function (){
        //commit immediatly if single row, otherwise press OK button to commit.
        if(!this.options.delayPost){
          this.options.fieldDatasTemp[0].value = this.getEditorValue();
        }
      },

    _appendRow: function(fieldDatasTemp, rowNum) {
      var editor = this;
        $('<li><a href="#">' + this._formatRowHtml(fieldDatasTemp) + '</a></li>')
        .data("row", rowNum)
        .appendTo(this.options.$editor.find("ul"))
        .click(function(){ editor._onRowClick($(this).data("row")); });
    },

    _onRemoveButtonClick: function(){
      var editor = this;
      $.each(this.options.$editor.find("ul li"), function(i) {
        if($(this).data("row") == editor.options.fieldDatasTemp[0].row) {
          if(confirm("Are you sure to delete?")){
            $(this).remove();

            $.each(editor.options.fieldDatasTemp, function(i) {
              this.status = "D";
            })

            editor._postData(gFormData.fields);
            //editor._postData(editor.options.fieldDatas);
            editor._onAddButtonClick();
          }
        }
      });
    },

    _onAddButtonClick: function() {
      this.clearEditorValue();
      this.options.fieldDatasTemp = this._initFieldDatasTemp();
      this.options.editorStatus = "new";
    },

      _onSaveButtonClick: function(){
        var editor = this;
        if(this.options.editorStatus == "new") {
          var rowNum = getNextRowNum(editor.options.fieldDatas, this.options.fieldTemplate.id);
        $.each(editor.options.fieldDatasTemp, function(i){
          this.value = editor.getEditorValue(this);
          this.row = rowNum;
        });

          this._postData(gFormModel.formData.fields);
          this._postData(editor.options.fieldDatas);
          this._appendRow(editor.options.fieldDatasTemp, rowNum);
          this._refreshListView();
          this._onAddButtonClick();
        } else {
          var $targetLi;
          $.each(this.options.$editor.find("ul li"), function(i){
                $li = $(this);
                $.each(editor.options.fieldDatasTemp, function(i) {
                  if($li.data("row") == this.row){
                    this.value = editor.getEditorValue(this);
                    $targetLi = $li;
                  }
                });
            });
          $targetLi.find("a").html(editor._formatRowHtml(editor.options.fieldDatasTemp));
          this._postData(gFormModel.formData.fields);
        }
      },

      _onRowClick: function(rowNum) {
      this.options.editorStatus = "edit";
      this.options.fieldDatasTemp = [];
      var fieldDatas = findDataByRow(this.options.fieldDatas, this.options.fieldTemplate.id, rowNum);
      $.extend(this.options.fieldDatasTemp, fieldDatas);
      this.setEditorValue(this.options.fieldDatasTemp);
    },

    _refreshListView: function() {
      this.options.$editor.find("ul").listview('refresh');
    },

    _postData: function(target){
      $.each(this.options.fieldDatasTemp, function(i) {
        if(this.row > -1){
          var field = findFieldById(target, this.id, this.row, this.col);
          if(field){
            if(this.status == "A") {
              this.status = "A";
            } else if(this.status == "D") {
              this.status = "D";
            } else {
              this.status = "M";
            }

            $.extend(field, this);
          }else{
            this.status = "A";
            target.push($.extend({}, this));
          }
        }
      });
    },

    redrawEditor: function() {
      this.options.$editor.remove();
      var $editor = this._drawEditor();
      this.options.$container.append($editor);
    },

    _translateDataSource: function(fieldData) {
      var editor = this;
      var result = fieldData.value;
      $.each(editor.options.fieldTemplate.fields, function(i) {
        if(fieldData.col == this.col) {
          if(this.shortcut == 'imagebox') {
            result = '<img src="' + fieldData.value + '" height="80"/>';
          }

          if(this.ds != null){
            if($.isArray(fieldData.value)) {
              result = "";
              var dataSources = this.ds;
              $.each(fieldData.value, function(i) {
                var v = this;
                $.each(dataSources, function(i) {
                  if(this.key == v[0]) {
                    result += (i == 0) ? this.value : "; " + this.value;
                  }
                })
              })
            } else {
              $.each(this.ds, function(i) {
                if(this.key == fieldData.value) {
                  result = this.value;
                }
              })
            }
          }
        }
      });
      return result;
    },
  });
})
