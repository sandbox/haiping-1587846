$(function() {
  $.widget("xtraform.imagebox", $.xtraform.editor, {
    options: {
      $preview: null,
      canvas: null,
      canvas_context: null
    },

    _drawEditor: function() {
      var html = '<div data-role="fieldcontain"><label class="ui-input-text">' + this.options.fieldTemplate.label + ':</label>\
                 <div style="display:inline-block;">';

      if(navigator.camera) {
        html += '<div data-role="controlgroup" data-type="horizontal"><button id="choosePhoto">Select</button><button id="takePhoto">Take Photo</button></div>';
      } else {
        html += '<input id="choosePhoto" type="file"/></br><i>This device does not support camera.</i></br>';
      }
      html += '<img id="preview" width="160"/><br/><canvas height="500" width="500" style="display:none;"></canvas></div></div>';

      return $(html);
    },

    _afterDrawEditor: function() {
      this.options.$preview = this.options.$editor.find("img#preview");
      this.options.canvas = this.options.$editor.find('canvas')[0];
      this.options.canvas_context = this.options.canvas.getContext("2d");
    },

    _initFieldDatasTemp: function() {
      return [{'id':this.options.fieldTemplate.id,
          'row':this.options.fieldTemplate.row,
          'col':this.options.fieldTemplate.col,
          'value':null}];
    },

    _registerEvents: function() {
      var editor = this;
      if (navigator.camera) {
        editor.options.$editor.find('#takePhoto').click(function (element) {
                  navigator.camera.getPicture(function (imageData) {
                        editor.options.$preview[0].src = "data:image/jpeg;base64," + imageData;
                        editor._onValueChanged();
                  },
              function (message) {
              },
              { quality: 45,
                  allowEdit: true,
                  encodingType: Camera.EncodingType.JPEG,
                  targetWidth: 500,
                  targetHeight: 500,
                  sourceType: Camera.PictureSourceType.CAMERA,
                  destinationType: Camera.DestinationType.DATA_URL
              });
              });
        editor.options.$editor.find('#choosePhoto').click(function (element) {
                  navigator.camera.getPicture(function (imageData) {
                        editor.options.$preview[0].src = "data:image/jpeg;base64," + imageData;
                        editor._onValueChanged();
                  },
              function (message) {
              },
              { quality: 45,
                  allowEdit: true,
                  encodingType: Camera.EncodingType.JPEG,
                  targetWidth: 500,
                  targetHeight: 500,
                  sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
                  destinationType: Camera.DestinationType.DATA_URL
              });
              });
          } else {
              var canvas = this.options.canvas;
              var context = canvas.getContext("2d");

              editor.options.$editor.find('#choosePhoto').change(function (e) {
                  var files = e.target.files;

                  var img = document.createElement("img");
                  img.name = files[0]['name'];
                  img.type = files[0]['type'];

                  var reader = new FileReader();
                  reader.onload = (function (x) {
                      return function (e) {
                          x.onload = function () {
                              /*if (x.height > x.width) {
                              x.width *= 500.0 / x.height;
                              x.height = "500";
                              } else {
                              x.height *= 500.0 / x.width;
                              x.width = "500";
                              }
                              context.clearRect(0, 0, x.width, x.height);
                              context.drawImage(x, 0, 0, x.width, x.height);
                              */
                              var sourceX = 0;
                              var sourceY = 0;
                              var sourceWidth = x.width;
                              var sourceHeight = x.height;
                              var destWidth = sourceWidth;
                              var destHeight = sourceHeight;
                              var destX = canvas.width / 2 - destWidth / 2;
                              var destY = canvas.height / 2 - destHeight / 2;

                              context.drawImage(x, sourceX, sourceY, sourceWidth, sourceHeight, destX, destY, destWidth, destHeight);

                              var base64 = canvas.toDataURL("image/jpeg");
                              editor.options.$preview[0].src = base64;
                              editor.options.$preview[0].filename = x.name;
                              editor.options.$preview[0].filemime = x.type;
                              editor._onValueChanged();
                          }
                          x.src = e.target.result;
                      };
                  })(img);
                  reader.onerror = function (e) {
                      alert(e);
                  }
                  reader.readAsDataURL(files[0]);
              });
          }
    },

    _formatRowHtml: function(fieldDatas) {
      return '<img width="80" src="' + fieldDatas[0].value + '" style="box-shadow:1px 1px 5px gray;"/>';
    },

    setEditorValue: function(fieldDatas) {
      this.options.$preview[0].src = fieldDatas[0].value;
    },

    getEditorValue: function() {
      this.options.fieldDatas[0].filename = this.options.$preview[0].filename;
      this.options.fieldDatas[0].filemime = this.options.$preview[0].filemime;
      return this.options.$preview[0].src;
    },

    clearEditorValue: function() {
      this.options.$preview[0].src = "";
      //this.options.canvas_context.clearRect();
    },

    redrawEditor: function() {

    }
  });
});
