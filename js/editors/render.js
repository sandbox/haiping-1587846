function renderEditors($container, content, template) {
  $.each(template, function(i) {
    renderEditor($container,
      content[this.field_name] == undefined ? [] : content[this.field_name]['und'],
      this);
    }
  );
}

function renderEditor($container, data,  fieldTemplate) {
  var $editor = new Object();

  var options = {
    $container: $container,
    fieldDatas: data,
    fieldTemplate: fieldTemplate
  };

  switch (fieldTemplate.widget.type) {
    case "text_textarea_with_summary":
      $editor = $('<div editor="' + fieldTemplate.widget.type + '"/>').textarea(options);
      break;

    case "image_image":
      $editor = $('<div editor="' + fieldTemplate.widget.type + '"/>').imagebox(options);
      break;

    case "taxonomy_autocomplete":
      $editor = $('<div editor="' + fieldTemplate.widget.type + '"/>').textbox(options);
      break;

  }

  return $editor;
}

function getNextRowNum(fields, fieldId) {
  var rowNum = -1;
  for (var i = 0; i < fields.length; i++) {
    if(fields[i].id == fieldId) {
      rowNum = fields[i].row;
    }
  }

  return ++rowNum;
}

//Return new array with duplicate values removed
Array.prototype.unique =
  function() {
    var a = [];
    var l = this.length;
    for(var i = 0; i < l; i++) {
      for(var j = i + 1; j < l; j++) {
        // If this[i] is found later in the array
        if (this[i] === this[j]) {
          j = ++i;
        }
      }
      a.push(this[i]);
    }
    return a;
}
