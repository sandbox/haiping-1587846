$(function() {
  $.widget("xtraform.textarea", $.xtraform.editor, {
    options: {

    },

    _drawEditor: function() {
      var html = '<div data-role="fieldcontain">';
      html += '<label for="textarea">' + this.options.fieldTemplate.label + ':</label>';
      html += '<textarea name="textarea" id="textarea"></textarea>';
      html += '</div>';

      return $(html);
    },

    _initFieldDatasTemp: function() {
      return [{'id':this.options.fieldTemplate.id,
          'row':this.options.fieldTemplate.row,
          'col':this.options.fieldTemplate.col,
          'value':null}];
    },

    _registerEvents: function() {
      var that = this;
      this.options.$editor
        .find("#textarea").change(function() {
          that._onValueChanged();
        });
    },

    _formatRowHtml: function(fieldDatas) {
      return fieldDatas[0].value;
    },

    setEditorValue: function(fieldDatas) {
      if(fieldDatas[0].value == "null") {
        fieldDatas[0].value = "";
      }
      this.options.$editor.find("textarea").val(fieldDatas[0].value);
    },

    getEditorValue: function() {
      return this.options.$editor.find("textarea").val();
    },

    clearEditorValue: function() {
      this.options.$editor.find("textarea").val("");
    }
  });
});
