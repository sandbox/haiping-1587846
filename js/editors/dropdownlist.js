$(function() {
  $.widget("xtraform.dropdownlist", $.xtraform.editor, {
    options : {

    },

    _drawEditor : function() {
      var html = '<div data-role="fieldcontain"><label for="select-choice" class="select">' + this.options.fieldTemplate.name + ':</label><select name="select-choice" id="select-choice" ';

      if (this.options.fieldTemplate.attrs != null) {
        $.each(this.options.fieldTemplate.attrs, function(i) {
          html += this.name + '="' + this.value + '" ';
        });
      }
      html += '>';

      if (this.options.fieldTemplate.ds != null) {
        $.each(this.options.fieldTemplate.ds, function(i) {
          html += '<option value="' + this.key + '">' + this.value + '</option>';
        });
      }

      html += '</select></div>';

      return $(html);
    },

    _initFieldDatasTemp : function() {
      return [ {
        id : this.options.fieldTemplate.id,
        row : this.options.fieldTemplate.row,
        col : this.options.fieldTemplate.col,
        value : null
      } ];
    },

    _registerEvents : function() {
      var that = this;
      this.options.$editor.find("select").change(function() {
        that._onValueChanged();
      });
    },

    _formatRowHtml : function(fieldDatas) {
      return fieldDatas[0].value;
    },

    setEditorValue : function(fieldDatas) {
      this.options.$editor.find("select").val(
          this.options.fieldDatasTemp[0].value);
      // $("#select-choice").selectmenu().selectmenu('disable');
    },

    getEditorValue : function() {
      return this.options.$editor.find("select").val();
    },

    clearEditorValue : function() {
      this.options.$editor.find("input").val("");
    }
  });
});
