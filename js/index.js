var url = "/";
var currentFormType = 0;
var ajaxLoadingCount = 0;
var initialized = false;

function initSetup() {
  if (initialized) {
    return;
  }

  $.mobile.allowCrossDomainPages = true;
  $.mobile.defaultPageTransition = "slide";
  //$.mobile.selectmenu.prototype.options.nativeMenu = true;

    $.ajaxSetup({
      beforeSend: function(jqXHR, settings) {
        settings.url = url + settings.url;
        ajaxLoadingCount++;
        $.mobile.showPageLoadingMsg();
      },
      complete: function(jqXHR, textStatus) {
        ajaxLoadingCount--;
        if(!ajaxLoadingCount) {
          $.mobile.hidePageLoadingMsg();
        }
      },
      error: function (jqXHR) {
        if (jqXHR.status > 0) {
          alert(jqXHR.status);
        }
      }
    });
    initialized = true;
}

$('#page-form-types').live('pageinit', function(event) {
    initSetup();
        $(this).find("#refresh-form-types").click(function () {
          gCache.remove("types", function() {
            appendContentTypes();
          });
        });
    appendContentTypes();
  }
);

$('#page-forms').live('pageinit', function(event) {
    initSetup();
    $(this).find("#refresh-forms").click(function () {
       gCache.remove("nodes", function(){
          appendContentHeader();
       });
    });
    appendContentHeader();
  }
);

$('#page-options').live('pageinit', function(event) {
    $(this).find("#userAgent").html(navigator.userAgent);
      $(this).find("#clearCache").click(function () {
          if (confirm('Are you sure to clear data of local storage?')) {
            gCache.clear();
          }
      });
  }
);

function appendContentTypes() {
  getContentTypes(function(data){
    var $list = $("#page-form-types #list-form-types");
    $list.empty();
    $list.append('<li><div id="getStart">Get Started</div></li>');
    $.each(data, function(i){
      $('<li><a><h3>' + this.name + '</h3><p>' + this.description + '</p></a></li>')
      .data("data", this)
      .click(function(){
          var contentType = $(this).data("data");
          var contentData = addContent(contentType.type, contentType.name);
            getTemplate(contentType.type, function(template) {
              renderForm(contentData, template);
            });
      })
      .appendTo($list);
    });
    $list.listview('refresh');
  });
}

function appendContentHeader() {
  getNodes(function(data) {
    var $list = $("#page-forms #list-forms");
    $list.empty();
    $.each(data, function(i) {
      appendNodeRow($list, this);
    });
    $list.listview('refresh');
  });
}
