var XtraFormCache = function() {
  this.enable = true;

  this.set = function(key, value, callback) {
    if (!this.enable && $.isFunction(callback)) {
      callback();
      return;
    }

    var value = JSON.stringify(value);
    if ($.isFunction(window.requestFileSystem)) {
      window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(
          fileSystem) {
        fileSystem.root.getFile(key, {
          create : true
        }, function(fileEntry) {
          fileEntry.createWriter(function(writer) {
            writer.onwrite = function(evt) {
              if ($.isFunction(callback)) {
                callback();
              }
            };
            writer.write(value);
          }, function(error) {
            alert(error);
          });
        }, function(error) {
          alert(error);
        });
      }, function(error) {
        alert(error);
      });
    } else if ($.isFunction(window.localStorage.getItem)) {
      /**
       * http://arty.name/localstorage.html Go to about:config and search
       * for "dom.storage.default_quota".
       */
      window.localStorage.setItem(key, value);
      if ($.isFunction(callback)) {
        callback();
      }
    } else {
      this['data'][key] = value;
      if ($.isFunction(callback)) {
        callback();
      }
    }
  };

  this.get = function(key, onSuccess, onFailed) {
    if (!this.enable && $.isFunction(onFailed)) {
      onFailed();
      return;
    }

    if ($.isFunction(window.requestFileSystem)) {
      window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(
          fileSystem) {
        fileSystem.root.getFile(key, {
          create : true
        }, function(fileEntry) {
          fileEntry.file(function(file) {
            var reader = new FileReader();
            reader.onloadend = function(evt) {
              if (evt.target.result == "") {
                onFailed();
              } else {
                var data = JSON.parse(evt.target.result);
                onSuccess(data);
              }
            };
            reader.readAsText(file);
          }, function(error) {
            alert(error);
          });
        }, function(error) {
          alert(error);
        });
      }, function(error) {
        alert(error);
      });
    } else if ($.isFunction(window.localStorage.getItem)) {
      if (window.localStorage.getItem(key) == null) {
        onFailed();
      } else {
        var data = JSON.parse(window.localStorage.getItem(key));
        onSuccess(data);
      }

    } else {
      if (this['data'] == null) {
        this['data'] = new Array();
      }
      if (this['data'][key] == null) {
        onFailed();
      } else {
        var data = JSON.parse(this['data'][key]);
        onSuccess(data);
      }
    }
  };

  this.remove = function(key, callback) {
    if (!this.enable && $.isFunction(callback)) {
      callback();
      return;
    }

    if ($.isFunction(window.requestFileSystem)) {
      window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(
          fileSystem) {
        var dirReader = fileSystem.root.createReader();
        dirReader.readEntries(function(results) {
          for (var i = 0; i < results.length; i++) {
            if (results[i].name == key) {
              results[i].remove();
              break;
            }
          }
          if ($.isFunction(callback)) {
            callback();
          }
        }, function(error) {
          alert(error);
        });
      }, function(error) {
        alert(error);
      });
    } else if ($.isFunction(window.localStorage.removeItem)) {
      window.localStorage.removeItem(key);
      if ($.isFunction(callback)) {
        callback();
      }
    } else {
      this['data'][key] = null;
      if ($.isFunction(callback)) {
        callback();
      }
    }
  };

  this.clear = function() {
    if (!this.enable) {
      return;
    }

    window.localStorage.clear();
    gCache['data'] = null;
    if ($.isFunction(window.requestFileSystem)) {
      window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(
          fileSystem) {
        var dirReader = fileSystem.root.createReader();
        dirReader.readEntries(function(results) {
          for (var i = 0; i < results.length; i++) {
            results[i].remove();
          }
        }, function(error) {
          alert(error);
        });
      }, function(error) {
        alert(error);
      });
    }
  };
}

var gCache = new XtraFormCache();
gCache.enable = false;
