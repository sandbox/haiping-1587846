function getContentTypes(complete) {
  var cacheId = "types";
  gCache.get(cacheId, complete, function() {
      $.post("xtraform/types",
        function(data) {
          complete(data);
          gCache.set(cacheId, data);
        }
      );
    }
  );
}

function addContent(contentType, title) {
  var timestamp = new Date();
  var unixTimestamp = Math.round(timestamp.getTime() / 1000);

  var content = {
    nid: 0 - unixTimestamp,
        title: title + " " + unixTimestamp,
        type: contentType,
        created: unixTimestamp
  };

  return content;
}

function renderForm(content, template) {
  var cacheId = "page_" + content.type;
  $("#" + cacheId).remove();
  var page = '<div id="' + cacheId + '" data-role="page" data-title="' + content.title + '">';
  page += '<div data-role="header" data-tap-toggle="false" data-position="fixed">';
  page += '<a id="back" href="#" data-icon="arrow-l">Back</a>';
  page += '<h1>Edit ' + content.title + '</h1>';
  page += '<a id="refreshForm" href="#" data-icon="refresh" data-theme="a">Refresh</a>';
  page += '</div>';
  page += '<div id="content" data-role="content"></div>';
  page += '<div data-role="footer" data-id="main-footer" class="ui-bar" data-position="fixed" data-tap-toggle="false">';
  page += '<div data-role="controlgroup" data-type="horizontal">';
  page += '<a id="deleteForm" href="#" data-icon="delete">Delete</a>';
  page += '<a id="copyForm" href="#" data-icon="plus">Copy</a>';
  page += '<a id="saveForm" href="#" data-icon="check">Save</a>';
  page += '</div>';
  page += '</div>';
  page += '</div>';
  var $page = $(page);
  $page.appendTo("body");

  $page.find("#saveForm")
     .click(function() {
       saveNode(content);
      }).end()
     .find("#refreshForm")
     .click(function() {
       refreshForm(content);
      }).end()
     .find("#copyForm")
     .click(function() {
       self.copyForm(content);
      }).end()
     .find("#deleteForm")
     .click(function() {
       deleteNode(content);
      }).end()
      .find("#back")
      .click(function() {
        var backUrl = (location.hash == "") ? "#page-form-types" : location.hash;
        $.mobile.changePage("#page-forms", {reverse: true});
      });

  renderEditors($page.find("[data-role=content]"), content, template);

  $.mobile.changePage($page);
}

function getTemplate(contentType, complete) {
    var cacheId = "template_" + contentType;
  gCache.get(cacheId, complete, function() {
      $.post("xtraform/template/" + contentType,
        function(data) {
          complete(data);
          gCache.set(cacheId, data);
      });
    }
  );
}

function refreshForm(node){
  gCache.remove("node_" + node['nid'], function(){
    $.mobile.changePage($("#page-forms"));
  });
}

function getNodes(complete) {
  var cacheId = "nodes";
  gCache.get(cacheId, complete, function(data) {
      $.post("xtraform/nodes",
        function(data) {
          complete(data);
          gCache.set(cacheId, data);
        }
      );
    }
  );
}

function appendNodeRow($container, node){
  var timestamp = new Date(node.created * 1000);
  var html = '<li><a href="#"><h3>' + node.title + '</h3>';
  html += '<p><strong>' + node.description + '</strong></p>';
  html += '<p class="ui-li-aside">' + timestamp.toLocaleString() + '</p>';
  html += '</a></li>';
  $(html)
    .data("data", node)
    .prependTo($container)
    .click(function() {
      var node = $(this).data("data");
      getNodeData(node.nid, function(contentData) {
            getTemplate(node.type, function(template) {
              renderForm(contentData, template);
            });
        }
      );
    }
  );
}

function getNodeData(nid, complete) {
  var cacheId = "node_" + nid;
  gCache.get(cacheId, complete, function(data) {
      $.post("xtraform/node/" + nid,
        function(data) {
          complete(data);
          gCache.set(cacheId, data);
        }
      );
    }
  );
}

function saveNode(content, complete) {
  var cacheId = "node_" + content.nid;
  if(navigator.onLine) {
    $.post('xtraform/save_node/' + content.nid,
      {"data": content},
      function(data) {
        //update cache
        gCache.remove(cacheId, function() {
            gCache.set(cacheId, content);
          }
        );
      }
    );
  } else {

  }
}

function deleteNode(content) {
  if(confirm("Are you sure to delete?")) {
    $.post("xtraform/delete_node/" + content['nid'],
      function(data){

      }
    );
  }
}
