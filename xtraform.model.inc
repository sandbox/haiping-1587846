<?php

/**
 * @file
 * model
 */

/**
 * Get all content types.
 */
function xtraform_get_node_types() {
  $node_types = node_type_get_types();
  drupal_json_output($node_types);
}

/**
 * Get content type definition.
 */
function xtraform_get_content_template($content_type) {
  $field_instances = field_info_instances('node', $content_type);
  drupal_json_output($field_instances);
}

/**
 * Get image base64.
 */
function xtraform_get_image_base64($fid) {
  $image = "";
  $file = file_load($fid);
  $style = image_style_load('thumbnail');
  $derivative_uri = image_style_path($style['name'], $file->uri);
  $success = file_exists($derivative_uri) || image_style_create_derivative($style, $file->uri, $derivative_uri);
  if ($success) {
    $scheme = file_uri_scheme($derivative_uri);
    // Transfer file in 1024 byte chunks to save memory usage.
    if ($scheme && file_stream_wrapper_valid_scheme($scheme) && $fd = fopen($derivative_uri, 'rb')) {
      while (!feof($fd)) {
        $image .= fread($fd, 1024);
      }
      fclose($fd);
    }
  }
  return ($image == "") ? "" : 'data:image/jpeg;base64,' . base64_encode($image);
}

/**
 * Save node.
 */
function xtraform_save_node($nid) {
  global $user;
  $data = $_REQUEST['data'];
  $node = (object) $data;
  $node_original = node_load($nid);
  foreach (field_info_instances('node', $node->type) as $field_instance) {
    $field = &$node->$field_instance['field_name'];
    if ($field == NULL) {
      continue;
    }

    $field = &$field[LANGUAGE_NONE];

    $field_original = $node_original->$field_instance['field_name'];
    $field_original = $field_original[LANGUAGE_NONE];

    $module = $field_instance['widget']['module'];
    if ($module == 'image') {
      if ($field[0]['filename'] != $field_original[0]['filename']) {
        xtraform_save_image($field[0]['fid'], $field[0]['filename'], $field[0]['filemime'], drupal_substr($field[0]['value'], 23));
      }
    }
    elseif ($module == 'taxonomy') {
      $term = taxonomy_term_load($field_original[0]['tid']);
      if ($field[0]['value'] != $term->name) {
        $field[0]['tid'] = 'autocreate';
        $field[0]['vid'] = $term->vid;
        $field[0]['vocabulary_machine_name'] = $term->vocabulary_machine_name;
        $field[0]['name'] = $field[0]['value'];
      }
    }
  }
  node_save($node);
  drupal_json_output($node->nid);
}

/**
 * Save image.
 */
function xtraform_save_image($fid, $file_name, $filemime, $base64) {
  global $user;

  $source = imagecreatefromstring(base64_decode($base64));
  $uri = 'public://field/image/' . $file_name;
  $realpath = drupal_realpath($uri);
  imagejpeg($source, $realpath);
  imagedestroy($source);
  drupal_chmod($realpath);
  $file = new stdClass();
  if ($fid > 0) {
    image_path_flush($uri);
    $file->fid = $fid;
  }
  $file->uid      = $user->uid;
  $file->status   = 0;
  $file->filename = $file_name;
  $file->uri      = $uri;
  $file->destination = $uri;
  $file->filemime = $filemime;
  $file = file_save($file);
  if ($fid < 0) {
    db_update('file_managed')
      ->fields(array('status' => 1))
      ->condition('fid', $file->fid)
      ->execute();
  }
  return $file->fid;
}

/**
 * Get all nodes.
 */
function xtraform_get_nodes() {
  $nodes = array();
  $result = db_select('node', 'n')
    ->fields('n', array('nid', 'type', 'title', 'created', 'uid'))
    ->execute();
  while ($row = $result->fetchAssoc()) {
    $user = user_load($row['uid']);
    $row['description'] = $user->name;
    $nodes[] = $row;
  }
  return drupal_json_output($nodes);
}

/**
 * Get node detail.
 */
function xtraform_get_node_data($nid) {
  $node = node_load($nid);
  foreach (field_info_instances('node', $node->type) as $field_instance) {
    $field = &$node->$field_instance['field_name'];
    $field = &$field[LANGUAGE_NONE];
    $module = $field_instance['widget']['module'];
    if ($module == 'image') {
      $field[0]['value'] = xtraform_get_image_base64($field[0]['fid']);
    }
    elseif ($module == 'taxonomy') {
      $term = taxonomy_term_load($field[0]['tid']);
      $field[0]['value'] = $term->name;
    }
  }
  return drupal_json_output($node);
}

/**
 * Delete node.
 */
function xtraform_delete_node($nid) {
  $node = node_load($nid);
  if (node_access('delete', $node)) {
    node_delete($nid);
  }
  drupal_json_output($nid);
}
